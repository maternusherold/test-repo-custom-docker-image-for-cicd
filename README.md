# Testing Setting Up A Custom Docker Image for CI/CD

When building the ci/cd pipeline, often most of the time is required to modify a docker image to the needs of the deployment. This includes installing packages etc. To reduce the time needed for setup, a custom docker image can be pushed to a private registry, hosted within the GitLab instance. 

The idea is to build some random base image, which usually has a long build-time. 
This would become a problem when used with multiple stages. 

In this example, `torch`, `torchvision` and `torchaudio` will be installed in an 
Ubuntu image.

In this case, setting up the container took 8 minutes which could be reduced to a 
couple of seconds, needed for pulling the one image from the registry.

![img.png](resources/exec_time_image_creation.png)


## Steps 

 1. Setup ci/cd pipeline using `.gitlab-ci.yml`
 2. Add `Dockerfile` describing custom image to be build 
 3. Add `build image` stage to pipeline which builds the image and pushes it 
    to the registry. Get the commands and variables for this from `Packages & 
    Registries > Container 
    Regsitry`
    1. login to registry: `docker login -u $CI_REGISTRY_USER $CI_REGISTRY`
    2. access the password via stdin rather than inputting it directly to prevent 
       warnings: `echo $CI_REGISTRY_PASSWORD | ... --password-stdin`
 4. Build the image: `docker build -t $CI_REGISTRY_IMAGE .`
 5. Push the pipeline file: `$CI_REGISTRY_IMAGE`
 6. _optional:_ add the following to the build stage to only build the image on 
    certain changes
    ```yaml
      only:
        changes:
          - ".gitlab-ci.yml"
          - "Dockerfile"
    ```


A [list and descriptions](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html) of the predefined variables available in GitLab for CD/CD. 
