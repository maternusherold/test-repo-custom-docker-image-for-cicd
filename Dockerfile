FROM ubuntu:focal
RUN apt update &&\
    apt install python3-pip -y &&\
    python3 --version &&\
    pip3 install torch torchvision torchaudio
